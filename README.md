# Ansible Role: Magneto 2 Setup

Create magento project root directory, copy app/etc/env.php and composer auth.json files.

## Requirements

This role require Ansible 2.9 or higher.

This role was designed for:

- RHEL/CentOS 7/8

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    magento_webuser_name: "www-data"
    magento_webuser_home: "/var/www/"

Magento project root directory config variables.

    magento_env_enabled: false
    magento_env_path: "/var/www/shared/app/etc/env.php"
    magento_env_template_src: env.php.j2
    magento_env_admin_path: admin
    magento_env_crypt_key: ""
    magento_env_mage_mode: 'default'

Main config variables for app/etc/env.php.

    magento_env_db_host: "127.0.0.1"
    magento_env_db_dbname: ""
    magento_env_db_username: ""
    magento_env_db_password: ""

Database config variables for app/etc/env.php.

    magento_env_redis_session_host: "127.0.0.1"
    magento_env_redis_session_port: "6379"
    magento_env_redis_session_password: ""
    magento_env_redis_session_db: 0
    magento_env_redis_session_max_concurrency: "30"
    magento_env_redis_cache_host: "127.0.0.1"
    magento_env_redis_cache_port: "6379"
    magento_env_redis_cache_password: ""
    magento_env_redis_cache_default_db: 1
    magento_env_redis_cache_page_cache_db: 2
    magento_env_redis_cache_id_prefix: ""

Redis config variables for app/etc/env.php.

    magento_env_rabbitmq_enabled: false
    magento_env_rabbitmq_host: "127.0.0.1"
    magento_env_rabbitmq_port: "5672"
    magento_env_rabbitmq_username: ""
    magento_env_rabbitmq_password: ""

RabbitMQ config variables for app/etc/env.php.

	magento_env_document_root_is_pub: "true"
    magento_env_downloadable_domains: "example.com"
    magento_env_install_date: "Tue, 11 August 2020 12:03:21 +0000"
    magento_env_http_cache_host: ""
    magento_env_http_cache_port: "6081"
    magento_env_mage_indexer_threads_count: "1"
    magento_env_cron_consumers_runner:
        cron_run: true
        max_messages: 10000
        consumers: []

Other config variables for app/etc/env.php.

    magento_composer_auth_template_src: ""
    magento_composer_home_path: "{{ magento_webuser_home }}/.composer"
    magento_composer_home_owner: "{{ magento_webuser_name }}"
    magento_composer_home_group: "{{ magento_webuser_name }}"

Composer auth.json file variables.

## Dependencies

No.

## License

MIT / BSD

## Author Information

This role was created in 2020 by [Maksim Soldatjonok](https://www.maksold.com/).
